#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

# Pre build
_sh "apt-get update"
_sh "apt-get install --yes wget"

_sh "wget ${BUILD_ARG_REGION_URL} --output-document=./region.osm.pbf"
_sh "osrm-extract -p /opt/car.lua /build/region.osm.pbf"
_sh "osrm-partition /build/region.osrm"
_sh "osrm-customize /build/region.osrm"
_sh "rm ./region.osm.pbf"

# Post build
_sh "apt-get clean -y"
_sh "rm -rf /var/cache/debconf/*"
_sh "rm -rf /var/lib/apt/lists/*"
_sh "rm -rf /var/log/*"
_sh "rm -rf /tmp/*"
_sh "rm -rf /var/tmp/*"
_sh "rm -rf /build.d/"
